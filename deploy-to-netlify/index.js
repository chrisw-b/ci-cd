const core = require("@actions/core");
const fs = require("fs");
const slugify = require("slugify");
const { exec } = require("child_process");

slugify.extend({ "/": "-" });

const botId = 36;

// env variables
const githubApiUrl = core.getInput("GITHUB_API_URL", { required: true });
const githubRepo = core.getInput("GITHUB_REPOSITORY", { required: true });
const gitSHA = core.getInput("GITHUB_SHA", { required: true });

// Secrets
const giteaToken = core.getInput("GITEA_TOKEN", { required: true });

// Inputs
const context = core.getInput("NETLIFY_CONTEXT", { required: true });
const prNumber = core.getInput("PR_NUMBER");
const sourceBranch = core.getInput("SOURCE_BRANCH", { required: true });
const branchSlug = slugify(sourceBranch);

const prIssueCommentsUrl = `${githubApiUrl}/repos/${githubRepo}/issues/${prNumber}/comments`;
const alias = `${prNumber}--${branchSlug}`.substring(0, 30);

const postComment = async (uploadData) => {
  const response = await fetch(prIssueCommentsUrl);
  const data = await response.json();
  const hasComment = data?.some((c) => c.user.id === botId);

  if (!hasComment) {
    const comment = `
  Successfully deployed to Netlify!

  The changes can be previewed here: ${uploadData.deploy_url}

  [Logs](${uploadData.logs})

  > Beep boop, I'm a bot.
  `;

    const commentResponse = await fetch(prIssueCommentsUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `token ${giteaToken}`,
      },
      body: JSON.stringify({ body: comment }),
    });

    if (commentResponse.ok) {
      console.log("Successfully posted comment!");
    } else {
      console.error(commentResponse.status, commentResponse.statusText);
    }
  }
};

const promiseExec = async (cmd) => {
  return new Promise((resolve, reject) => {
    const child = exec(cmd);

    let result = "";
    child.stdout.on("data", (message) => {
      result += message.toString();
    });

    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);

    child.on("close", (code) => {
      if (code != 0) reject();
      else resolve(result);
    });
  });
};

(async function () {
  await promiseExec(`./node_modules/.bin/netlify build --context ${context}`);
  const output = await promiseExec(
    `./node_modules/.bin/netlify deploy \
    --message "SHA: ${gitSHA}" \
    --json \
    ${context === "production" ? "-p" : `--alias ${alias}`}`,
  );

  const uploadData = JSON.parse(output);

  core.setOutput("preview_url", uploadData.deploy_url);

  if (context !== "production") {
    await postComment(uploadData);
  }
})();
